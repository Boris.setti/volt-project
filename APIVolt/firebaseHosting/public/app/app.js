const URL = 'https://us-central1-gpsforvolt.cloudfunctions.net/'

const loading = (status) => {
    if (status == true) {
        $('.tabResult').before('<div class="loading">Requête en cours</div>')
        i = 0;
        setInterval(function () {
            i = ++i % 10;
            $(".loading").text("Requête en cours " + Array(i + 1).join("."));
        }, 500);
    }else
    $("div").remove(".loading")
}
$('#postData').on('click', () => {

    $("p").remove(".deletePost")

    axios.post(URL + 'api/locations', {
        userId: $('#userId').val(),
        date: $('#date').val(),
        latitude: $('#latitude').val(),
        longitude: $('#longitude').val(),
        battery: $('#battery').val(),
        speed: $('#speed').val(),
        direction: $('#direction').val(),
    })
        .then((res) => {
            $('.postData').append(`<p class='deletePost'>${res.data.success ? "locations envoyer" : "une erreur s'est produite lors de l'envoi " + JSON.stringify(res.data)}</p>`)
        })

})

$('#getData').on('click', () => {
    $('.tabResult').css('display', 'none')
loading(true)
    $("tr , div").remove(".delete")

    axios.get(URL + 'api/locations', {
        params: {
            locationNumber: $('#locationNumber').val()
        }
    })
        .then((res) => {
            let result = res.data.locations
            if (res.data.success) {
                loading(false)
                $('.tabResult').css('display', 'contents')
                result.forEach((value) => {
                    $('.tabGetResult').append(`
                    <tr class='delete'>
                        <td>${value.date}</td>
                        <td>${value.latitude}</td>
                        <td>${value.longitude}</td>
                        <td>${value.direction}</td>
                        <td>${value.speed}</td>
                        <td>${value.battery}</td>
                    </tr>
                    `)
                });
            } else
                $("#btnGetData")
                    .append(`
                        <div class="delete">
                        <p>${"une erreur s'est produite lors de l'envoi " + JSON.stringify(res.data)}</p>
                        </div>
                    `)

        })
})