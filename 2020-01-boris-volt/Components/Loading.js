import React, { Component } from "react";
import {
    View,
    ActivityIndicator,
    StyleSheet
} from "react-native";


export default class Loading extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false
        }
    }


    displayLoading = () => {
        if (this.props.isLoading) {
            return (
                    <ActivityIndicator size={100} />
            )
        }
    }


    render() {
        return (
            <View style={styles.loading_container}>
            { this.displayLoading() }
            </View>
        )
    }

}

const styles = StyleSheet.create({
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 200,
        alignItems: 'center',
        justifyContent: 'center'
    }
});