import React, { Component } from "react";
import Geolocation from "react-native-geolocation-service";
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import { requestLocationPermission } from '../Pemission/Location';
import { startLog, addLog, stopLog } from './CreateLog'
import CurrentData from "./CurrentData";
import {
    StyleSheet,
    View,
    Alert,
    PermissionsAndroid,
} from "react-native";
import ShowLog from "./ShowLog";
import OptionButton from "./OptionButton"
import FunctionButton from "./FunctionButton";
import { createLogFileIfNotExist } from './ToolBox'
import Loading from './Loading'


export default class GPS extends Component {

    constructor(props) {
        super(props)
        this.watchId = -1

        this.state = {
            watch: 0,// état du GPS
            enableHighAccuracy: true, // option gps
            distanceFilter: 20,  // option gps
            fastestInterval: 3000,  // option gps
            position: null,  //GPS
            speedKMH: null,  //GPS
            isLoading: false
        };
    }

    componentDidMount() {
        BackgroundGeolocation.configure({
            notificationTitle: 'GPS En Arrière-plan Volt ',
            notificationText: 'Activé',
            debug: false,
            postTemplate: null,
        });
    }

    //--------------------------------------------------GPS----------------------------------------------------------------//

    startGPS = () => {
        PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
            .then(res => {
                if (res == false) {
                    requestLocationPermission()
                } else {
                    if (this.state.watch == 0) {
                        createLogFileIfNotExist()
                        this.setState({ 
                            watch: this.state.watch + 1,
                            isLoading: true
                         })
                        startLog(this.state.distanceFilter, this.state.fastestInterval)
                        setTimeout(() => {
                            BackgroundGeolocation.start(
                                this.watchId = Geolocation.watchPosition(
                                    position => {
                                        speed = position.coords.speed * 3.6
                                        speedKMH = speed.toFixed(2)
                                        this.setState({
                                            position,
                                            speedKMH,
                                            watch: this.state.watch + 1,
                                            isLoading: false
                                        })
                                        addLog(position)
                                    },
                                    error => Alert.alert(error.message),
                                    {
                                        enableHighAccuracy: this.state.enableHighAccuracy, //Utiliser le mode haute précision
                                        distanceFilter: this.state.distanceFilter, //Déplacement minimum entre les mises à jour de l'emplacement en mètres
                                        interval: this.state.fastestInterval, // Intervalle pour les mises à jour de localisation actives en ms (Android uniquement)
                                        fastestInterval: this.state.fastestInterval, //Taux le plus rapide auquel votre application recevra les mises à jour de localisation, qui peut être plus rapide que interval dans certaines situations (par exemple, si d'autres applications déclenchent des mises à jour de localisation) (Android uniquement)
                                        forceRequestLocation: false, //Forcer l'emplacement de la demande même après le refus Améliorer la boîte de dialogue de précision (Android uniquement)
                                    }
                                )
                            )
                        }, 2000)
                    }
                }
            })
    };


    stopGPS = () => {
        this.setState({
            watch: this.state.watch + 1,
            isLoading: true
        })
        BackgroundGeolocation.stop()
        Geolocation.clearWatch(this.watchId);
        this.watchId = -1
        stopLog()
        setTimeout(() => {
            this.setState({
                watch: 0,
                position: null,
                speedKMH: null,
                isLoading: false
            })
        }, 1500)
    }

    //-----------------------------------------------------------------------button option----------------------------------------------------

    rebootGPS = () => {
        if (this.state.watch != 0) {
            this.stopGPS()
            setTimeout(() => {
                this.startGPS()
            }, 2000)
        }
    }

    handleChangeHighAccuracy = (value) => {
        this.setState({ enableHighAccuracy: value })
        this.rebootGPS()
    }

    handleChangeIntevalMetre = (value) => {
        this.setState({ distanceFilter: parseFloat(value) })
        this.rebootGPS()
    }

    handleChangeIntevalSeconde = (value) => {
        this.setState({ fastestInterval: parseFloat(value) * 1000 })
        this.rebootGPS()
    }

    render() {
        return (
            <View style={styles.container}>

                <OptionButton
                    data={{
                        enableHighAccuracy: this.state.enableHighAccuracy,
                        distanceFilter: this.state.distanceFilter,
                        fastestInterval: this.state.fastestInterval,
                        handleChangeIntevalMetre: this.handleChangeIntevalMetre.bind(),
                        handleChangeIntevalSeconde: this.handleChangeIntevalSeconde.bind(),
                        handleChangeHighAccuracy: this.handleChangeHighAccuracy.bind()
                    }} />
                <FunctionButton data={{
                    startGPS: this.startGPS.bind(),
                    stopGPS: this.stopGPS.bind(),
                    watch: this.state.watch
                }}
                />

                <CurrentData position={this.state.position} speedKMH={this.state.speedKMH} />

                <ShowLog
                    data={{
                        watch: this.state.watch,
                        distanceFilter: this.state.distanceFilter,
                        fastestInterval: this.state.fastestInterval,
                    }} />
                <Loading  isLoading={this.state.isLoading}/>
            </View >
        );
    }


}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5FCFF"
    }
});


