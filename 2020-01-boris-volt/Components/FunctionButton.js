import React, { Component } from "react";
import RNFS from 'react-native-fs';
import {
    createLogFileIfNotExist
} from './ToolBox'
import {
    StyleSheet,
    View,
    Alert,
    Button,
} from "react-native";

export default class FunctionButton extends Component {
    constructor(props) {
        super(props)
        this.path = RNFS.CachesDirectoryPath + '/DataLog.txt'
    }

    pressButtonInfoLog = () => {
        RNFS.readFile(this.path, 'utf8')
        .then(res => {
            Alert.alert(
                'Log Info',
                `Il y a actuellement ${JSON.parse(res).length} log.`,
                [
                    {
                        text: 'OK',
                        onPress: () => {},
                                            },
                    { text: 'Supprimer', onPress: () => this.pressButtonSuppLog() },
                ],
                { cancelable: false },
            );
        })
    }


    pressButtonSuppLog = () => {
        Alert.alert(
            'Supprimer les log',
            'Êtes vous sur de vouloirs supprimer les logs ? Cette action et definitive.',
            [
                {
                    text: 'Cancel',
                    onPress: () => {},
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => this.suppLog() },
            ],
            { cancelable: false },
        );
    }

    suppLog = () => {
        RNFS.unlink(this.path)
        setTimeout(()=>{
            createLogFileIfNotExist()
        },500)
    }


    buttonStartStop = () => {
        if (this.props.data.watch == 0) {
            return ([
                <Button key={'startB'} title='start info GPS' color='green' onPress={this.props.data.startGPS} />,
                <Button key={'deleteB'} title='info log' color='#1aa3ff' onPress={this.pressButtonInfoLog} />,
            ])

        } else {
            return (
                <Button key={'stopB'} title='stop info GPS' color='red' onPress={this.props.data.stopGPS} />
            )
        }
    }

    render() {
        return (
            <View style={styles.startStop}>
                {this.buttonStartStop()}

            </View>
        )
    }
}

const styles = StyleSheet.create({

    startStop: {//<---------------aligne les button start & stop
        flex: 2,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: "center"
    }
});
