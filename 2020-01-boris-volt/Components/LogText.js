import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    View,
} from "react-native";


export default class LogText extends Component {


    //logText(mettre le titre de la en string valeur ex 'vitesse, la valeur, mettre l'unités de valeur ex 'km/h' en option )
    logText = (title, value, unity) => {
        let logTextJSX = null;
        if (value == null || value == undefined) { }
        else if (unity == undefined) {
            logTextJSX = <Text style={styles.textInfoGPS}>{`${title} : ${value}`}</Text>
        }
        else {
            logTextJSX = <Text style={styles.textInfoGPS}>{`${title} : ${value} ${unity}`}</Text>
        }
        return (
            logTextJSX
        )
    }
    render() {
        return (
            <View style={styles.infoGPS}>
                {this.logText(this.props.title, this.props.value, this.props.unity)}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textInfoGPS: {
        marginLeft: 10
    }
});