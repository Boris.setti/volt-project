import React, { Component } from "react";

import {
    StyleSheet,
    Text,
    View,
    Alert,
    Switch,
    TextInput,
} from "react-native";


export default class OptionButton extends Component {

    alertOptionInfo = () => {
        Alert.alert(
            'Erreur de saisie',
            `Vous deviez rentrer une valeur.`,
            [
                { text: 'OK' },

            ],

        );
    }

    render() {
        return (
            <View style={styles.switchOption}>
                <Text style={styles.title}>option</Text>

                <View style={styles.optionAndText}>
                    <Text>Mode haute précision :</Text>
                    <Switch
                        value={this.props.data.enableHighAccuracy}
                        onValueChange={(enableHighAccuracy) => {
                            this.props.data.handleChangeHighAccuracy(enableHighAccuracy)
                        }}
                    />
                </View>

                <View style={styles.optionAndText}>
                    <Text>Interval min de MAJ en mètre actuelle {this.props.data.distanceFilter}m:</Text>
                    <TextInput
                        style={styles.textinput}
                        placeholder="new"
                        keyboardType={"decimal-pad"}
                        onSubmitEditing={
                            ({ nativeEvent }) => {
                                isNaN(parseFloat(nativeEvent.text)) ?
                                    this.alertOptionInfo() :
                                    this.props.data.handleChangeIntevalMetre(nativeEvent.text)
                            }} />
                </View>

                <View style={styles.optionAndText}>
                    <Text>Interval Max de MAJ en Sec actuelle {this.props.data.fastestInterval / 1000}s:</Text>
                    <TextInput
                        style={styles.textinput}
                        placeholder="new"
                        keyboardType={"decimal-pad"}
                        minLength={1}
                        onSubmitEditing={
                            ({ nativeEvent }) => {
                                isNaN(parseFloat(nativeEvent.text)) ?
                                    this.alertOptionInfo() :
                                    this.props.data.handleChangeIntevalSeconde(nativeEvent.text)
                            }} />
                </View>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        textAlign: 'center'
    },
    optionAndText: { //<------------View qui align un text a son option 
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginRight: 10,
        marginTop: 3,
    },
    switchOption: {  //<------------Mes en colonne optionAndText
        flexDirection: 'column',
    },
    textinput: {
        backgroundColor: '#DCDCDC',
        padding: 0,
        width: 50
    }
});