import Geolocation from "react-native-geolocation-service";
import DeviceInfo from 'react-native-device-info';
import RNFS from 'react-native-fs';
import {
    calculateDifferenceDistance,
    calculateDifferenceTime
} from './ToolBox'


const path = RNFS.CachesDirectoryPath + '/DataLog.txt'

const writeFileDataLog = (dataLog, addFiles, location) => {
    const log = JSON.parse(dataLog)
    log.push(addFiles)
    RNFS.writeFile(location, JSON.stringify(log), 'utf8')
}

const idLog = (dataTab) => {
    const tabLog = JSON.parse(dataTab)
    if (tabLog.length < 1 || tabLog == undefined) {
        return 0
    }
    else {
        const last_element = tabLog[tabLog.length - 1]
        const startIdLog = last_element.id + 1
        return startIdLog
    }
}

export const startLog = (intervalMtr, intervalSec) => {

    RNFS.readFile(path, 'utf8')
        .then(res => {
            Geolocation.getCurrentPosition(
                position => {
                    const startWriteLog = {
                        'id': idLog(res),
                        "gps": "Start",
                        "timestamp": position.timestamp,
                        "latitude": position.coords.latitude,
                        "longitude": position.coords.longitude,
                        "intervalMtr": intervalMtr,
                        "intervalSec": intervalSec / 1000
                    }
                    writeFileDataLog(res, startWriteLog, path)
                })

        })

}

export const addLog = (position) => {

    DeviceInfo.getBatteryLevel()
        .then(batteryLevel => {
            const batterLevelInPourcentage = batteryLevel * 100
            RNFS.readFile(path, 'utf8')
                .then(res => {
                    const logWrite = {
                        "id": idLog(res),
                        "gps": "fonction",
                        "timestamp": position.timestamp,
                        "speedKMH": position.coords.speed * 3.6,
                        "lvBattery": batterLevelInPourcentage.toFixed(0),
                        "latitude": position.coords.latitude,
                        "longitude": position.coords.longitude,
                        "direction": position.coords.heading.toFixed(0)
                    }
                    writeFileDataLog(res, logWrite, path)
                })
        })

}

export const stopLog = () => {

    RNFS.readFile(path, 'utf8')
        .then(res => {
            const tabLog = JSON.parse(res)

            let distanceTotal = 0;
            let startPass = "no";
            let totalTimeDif = 0;
            let totalLoop = 0;

            for (let i = tabLog.length - 1; i >= 0; i--) {
                let tab = tabLog[i]
                let afterTab = tabLog[i - 1]
                if (tabLog[i].gps == 'Start') {
                    startPass = 'on'
                }
                else if (tab != undefined && afterTab != undefined && startPass == 'no') {
                    let distance = calculateDifferenceDistance([tab.latitude, tab.longitude], [afterTab.latitude, afterTab.longitude])
                    let timeDif = calculateDifferenceTime(tabLog[i].timestamp, tabLog[i - 1].timestamp)
                    distanceTotal = distanceTotal + distance
                    totalTimeDif = totalTimeDif + timeDif
                    totalLoop += 1

                } else { }
            }

            Geolocation.getCurrentPosition(
                position => {
                    const lat = position.coords.latitude
                    const lng = position.coords.longitude
                    const stopWriteLog = {
                        "id": idLog(res),
                        "gps": 'Stop',
                        "time": position.timestamp,
                        "latitude": lat,
                        "longitude": lng,
                        "distance": distanceTotal.toFixed(0),
                        "moyenTime": totalTimeDif / totalLoop,
                        "nbLog": totalLoop,
                    }
                    writeFileDataLog(res, stopWriteLog, path)
                })
        })


}