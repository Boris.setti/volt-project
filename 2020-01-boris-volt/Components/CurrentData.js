import React, { Component } from "react";
import {
    StyleSheet,
    View,
} from "react-native";
import LogText from "./LogText";


export default class CurrentData extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Direction: null,
            latitude: null,
            Longitude: null,
            Vitesse: null
        }
    }

    reloadState = () => {
        if (this.props.position == null) {
            this.setState({
                Direction: null,
                Latitude: null,
                Longitude: null,
                Vitesse: null
            })
        }
        else {
            this.setState({
                Direction: this.props.position.coords.heading.toFixed(0),
                Latitude: this.props.position.coords.latitude,
                Longitude: this.props.position.coords.longitude,
                Vitesse: this.props.speedKMH
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.position != this.props.position) {
            this.reloadState()
        }
    }


    render() {
        return (
                //LogText title={mettre le titre de la en string valeur ex 'vitesse'} value={la valeur} unity={mettre l'unités de valeur ex 'km/h' en option}
            <View style={styles.infoGPS}>
                <LogText title={'Direction'} value={this.state.Direction} unity={'°'} />
                <LogText title={'Latitude'} value={this.state.Latitude} />
                <LogText title={'Longitude'} value={this.state.Longitude} />
                <LogText title={'Vitesse'} value={this.state.Vitesse} unity={'Km/h'} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    infoGPS: {  //<-----------------View contenant les text infoGPS 
        flex: 3,
    }
});