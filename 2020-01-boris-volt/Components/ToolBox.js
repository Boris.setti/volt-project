import React from "react";
import turfDistance from '@turf/distance';
import RNFS from 'react-native-fs';
import moment from "moment";
import {
    Text,
} from "react-native";

const path = RNFS.CachesDirectoryPath + '/DataLog.txt'


export const createLogFileIfNotExist = () => {
    RNFS.exists(path).then(exists => {
        if (exists == false) {
            RNFS.writeFile(path, JSON.stringify([]), 'utf8')
        }
    })
}

export const timestampToDate = (timestamp) => {
    let unixTime = moment(timestamp).unix()
    let time = moment.unix(unixTime).format('DD/MM h:mm:ss')
    return time
}

export const calculateDifferenceValue = (value, previousValue) => {
    if (value != undefined && previousValue != undefined) {
        let result = value - previousValue
        return result
    }
    return null
}

export const calculateDifferenceTime = (unixTime, previousUnixTimes) => {
    let unixTimeAfter = moment(unixTime).unix()
    let unixTimeBefore = moment(previousUnixTimes).unix()
    let timeDif = moment(unixTimeAfter).diff(moment(unixTimeBefore));
    return timeDif
}

export const calculateDifferenceDistance = (tabPosition, tabPreviousPosition) => {
    var from = (tabPosition);
    var to = (tabPreviousPosition);
    var options = { units: 'meters' };
    var distance = turfDistance(from, to, options);
    return distance
}
export const createJSXTextElement = (id, backgroundColorText, colorText, value) => {
    let JSXElement = <Text key={id} style={{ backgroundColor: `${backgroundColorText}`, color: `${colorText}`, fontWeight: 'bold' }}>{value}</Text>
    return JSXElement
}