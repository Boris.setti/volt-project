import React, { Component } from "react";
import RNFS from 'react-native-fs';
import {
    timestampToDate,
    calculateDifferenceValue,
    calculateDifferenceTime,
    calculateDifferenceDistance,
    createJSXTextElement,
    createLogFileIfNotExist
} from './ToolBox'
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
} from "react-native";


export default class ShowLog extends Component {
    constructor(props) {
        super(props)
        this.path = RNFS.CachesDirectoryPath + '/DataLog.txt',
            this.state = {
                tabLog: [],
              }
    }


    formatStartLog = (oldTab, newValue) => {
        let time = timestampToDate(newValue.timestamp)
        //create JSX Text
        let startLog = createJSXTextElement(newValue.id, '#5cb85c', '#292b2c', `${time}   lat: ${newValue.latitude.toFixed(5)}   lng: ${newValue.longitude.toFixed(5)}`)
        let reglageStartLog = createJSXTextElement(newValue.id + 'a', '#5cb85c', '#292b2c', `Réglage: intervalle: ${newValue.intervalMtr} Mètres & ${newValue.intervalSec} Secondes`)

        oldTab = [...oldTab, startLog]
        oldTab = [...oldTab, reglageStartLog]
        return oldTab
    }

    formatStopLog = (oldTab, newValue) => {
        let time = timestampToDate(newValue.time)
        //create JSX Text
        let stopLog = createJSXTextElement(newValue.id, "#d9534f", '#292b2c', `${time}   lat: ${newValue.latitude.toFixed(5)}   lng: ${newValue.longitude.toFixed(5)}`)
        let infoStopLog = createJSXTextElement(newValue.id + "a", "#d9534f", '#292b2c', `Distance total ${newValue.distance}m   moyenne time ${newValue.moyenTime.toFixed(2)}s   nb log ${newValue.nbLog}`)

        oldTab = [...oldTab, stopLog]
        oldTab = [...oldTab, infoStopLog]
        return oldTab
    }

    formatLog = (oldTab, previousValue, newValue) => {

        let time = timestampToDate(newValue.timestamp)
        //calcule dif 
        var distance = calculateDifferenceDistance([newValue.latitude, newValue.longitude], [previousValue.latitude, previousValue.longitude])
        let timeDif = calculateDifferenceTime(newValue.timestamp, previousValue.timestamp)
        let batteryDif = calculateDifferenceValue(newValue.lvBattery, previousValue.lvBattery)
        let speedDif = calculateDifferenceValue(newValue.speedKMH, previousValue.speedKMH)
        //create JSX Text
        let showInfo = createJSXTextElement(newValue.id, "#f7f7f7", "#292b2c", `${time} ${newValue.speedKMH.toFixed(1)}Km/h ${newValue.direction}° ${newValue.lvBattery}% ${newValue.latitude.toFixed(6)} ${newValue.longitude.toFixed(6)}`)
        let showDif = createJSXTextElement(newValue.id + 'a', "#292b2c", "#f7f7f7", `Dist: ${distance.toFixed(1)}m    Time: ${timeDif}s    Speed: ${speedDif != null ? speedDif.toFixed(2) + "Km/h" : "Start"}    Bat: ${batteryDif != null ? batteryDif.toFixed(0) + "%" : "Start"}`)

        oldTab = [...oldTab, showInfo]
        oldTab = [...oldTab, showDif]

        return oldTab
    }

    showInfoLog = () => {

        RNFS.readFile(this.path, 'utf8')
            .then(res => {
                let tabLog = JSON.parse(res)
                if (tabLog.length < 1 || tabLog == undefined) {
                    let startWriteLog = <Text style={{ backgroundColor: "#5cb85c", fontWeight: 'bold', fontSize: 45 }}>Press Start GPS</Text>
                    this.setState({ tabLog: startWriteLog })
                }
                else {
                    let tabShowInfo = []
                    for (let i = tabLog.length - 1; i >= 0; i--) {
                        let tab = tabLog[i]
                        if (tab.gps == "Start") {
                            tabShowInfo = this.formatStartLog(tabShowInfo, tab)
                        } else if (tab.gps == 'Stop') {
                            tabShowInfo = this.formatStopLog(tabShowInfo, tab)
                        } else {
                            tabShowInfo = this.formatLog(tabShowInfo, tabLog[i - 1], tabLog[i])
                        }
                    }
                    this.setState({ tabLog: tabShowInfo })
                }
            });
    }


    componentDidMount() {
        createLogFileIfNotExist()
        setTimeout(() => {
            this.showInfoLog()
        }, 1500)
    }
    componentWillUnmount() {
        this.showInfoLog()
    }


    componentDidUpdate(prevProps) {
        if (prevProps.data.watch !== this.props.data.watch) {
            console.log('componentDidUpdate')
            this.showInfoLog()
       
        } 
    }



    render() {
        return (
            <View style={styles.log}>
                <ScrollView >
                    {this.state.tabLog}
                </ScrollView>
            </View>
        )
    }


}

const styles = StyleSheet.create({
    log: {
        flex: 12,
        backgroundColor: '#D1D1D1'
    },
    startStop: {//<---------------aligne les button start & stop
        flex: 2,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: "center"
    }
});