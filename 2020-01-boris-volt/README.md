# Readme GPS For Volt

## Prérequis
1. Avoir installé React-native `npm install -g react-native-cli`.
2. Installer un émulateur android ou avoir un smartphone android.
  * si émulateur android dans la doc [React Native](https://facebook.github.io/react-native/docs/getting-started) suivre le tuto React Native CLI Quickstart.
  * si smartphone android dans la doc [React Native](https://facebook.github.io/react-native/docs/running-on-device) suivre le tuto pour android.
  * passer votre smartphone en mode développeur. 
  
  ## Utilisation 
  1. Dans le dossier android crée un fichier `local.properties` et écrire `sdk.dir = /home/_VOTRE_NOM_D'UTILISATEUR_/Android/Sdk`
  2. `npm install`
  3. `npm start`
  _si vous utilisez un smartphone vous devez installé l'application, dans votre terminal tapé `react-native run-android`, je vous conseille de télécharger sur play store `lockito` pour Facker votre position GPS._