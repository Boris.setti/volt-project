var express = require("express");
const bodyParser = require("body-parser");


const app = express()
const path = require('path');
const data = require('./data')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


app.use(express.static('public'));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/addForm/addInfo.html'));
})

app.post('/api/locations', (req, res) => {
    let error = false
    let codeError = []
    let messageError = []
    let battery = null
    let speed = null
    let direction = null
    //------------------------test userId exist & format-----------------------------
    if (!req.body.userId) {
        error = true
        codeError.push('4001')
        messageError.push('userId missing')
    } else if (isNaN(req.body.userId)) {
        error = true
        codeError.push('4001')
        messageError.push('incorrect userId format')
    }
    //------------------------test date exist & format-----------------------------
    if (!req.body.date) {
        error = true
        codeError.push('4002')
        messageError.push('date missing')
    } else if (!/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/.test(req.body.date)) {
        error = true
        codeError.push('4002')
        messageError.push('incorrect date format')
    }
    //------------------------test latitude exist & format-----------------------------
    if (!req.body.latitude) {
        error = true
        codeError.push('4003')
        messageError.push('latitude missing')
    } else if (isNaN(req.body.latitude)) {
        error = true
        codeError.push('4003')
        messageError.push('incorrect latitude format')
    }
    //------------------------test longitude exist & format-----------------------------
    if (!req.body.longitude) {
        error = true
        codeError.push('4004')
        messageError.push('longitude missing')
    } else if (isNaN(req.body.longitude)) {
        error = true
        codeError.push('4004')
        messageError.push('incorrect longitude format')
    }
    //------------------------add option if exists------------------
    if (typeof req.body.battery != "undefined") {
        battery = req.body.battery
    }

    if (typeof req.body.speed != "undefined") {
        speed = req.body.speed
    }

    if (typeof req.body.direction != "undefined") {
        direction = req.body.direction
    }
    console.log(error)
    if (error == false) {
        let locations = {
            userId: req.body.userId,
            date: req.body.date,
            latitude: req.body.latitude,
            longitude: req.body.longitude,
            battery: battery,
            speed: speed,
            direction: direction
        }

        data.addData(locations, req.body.userId)
        res.json({ success: true, 'to send': locations })
    } else

        res.json({ success: false, 'code error': codeError, 'message error': messageError })
})



app.get('/api/locations', (req, res) => {
    let error = false
    let codeError = []
    let messageError = []
    let locationNumber = 10

    if (typeof req.query.locationNumber != "undefined") {
        if (isNaN(+req.query.locationNumber)) {
            error = true
            codeError.push('4010')
            messageError.push('incorrect locationNumber format')
        } else
            locationNumber = +req.query.locationNumber
    }
    if (locationNumber == 0) {
        locationNumber = 10
    } if (error == false) {
        data.getData(locationNumber).then((result) => {
            res.json({ success: true, 'locations': result })
        })
    } else
        res.json({ success: false, 'code error': codeError, 'message error': messageError })
})

app.listen(8080, () => console.log(`index.js lancé sur port 8080`));