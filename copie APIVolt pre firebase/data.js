var admin = require("firebase-admin");

var serviceAccount = require("./firebase-adminsdk.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://gpsforvolt.firebaseio.com"
});
let db = admin.firestore();

let users = {
  user: 'michel',
  email: 'mimi@wanadoo.fr',
}
let locations = {
  date: "2020-01-02T08:22:00",
  direction: "50",
  latitude: "1.45686",
  longitude: "2.45678",
  battery: "50",
  speed: "100"
}

const addUser = (user) => {
  let result = db.collection('users').get()
    .then((result) => {
      let userId = 1
      let userIdRegister = []

      result.forEach((doc) => {
        userIdRegister.push(+doc.data().id);
      });
      userIdRegister.sort((a, b) => a - b);
      userIdRegister.forEach((id) => {
        if (id == userId) {
          userId++
        }
      });

      let newUser = Object.assign(user, { id: userId });
      let docRef = db.collection('users').add(newUser);

      return docRef
    })
  return result
}

const addData = (location, userId) => {
  let addLocation = db.collection('locations')
    .orderBy('id', 'desc')
    .limit(1)
    .get()
    .then((result) => {
      let idLocation = 1;
      result.forEach((idLoc) => {
        if (!idLoc.data().id) {
          idLocation = 2
        } else
          idLocation = idLoc.data().id + 1
      });
      let addIdLocations = Object.assign({ id: idLocation }, location)
      let addLocations = Object.assign(addIdLocations, { userId: userId })
      let refDoc = db.collection('locations').add(addLocations);
      return refDoc
    })
  return addLocation
}

const getData = (locationNumber) => {



  return new Promise((resolve, reject) => {
    db.collection('locations')
      .orderBy('date', 'desc')
      .limit(locationNumber)
      .get()
      .then((result) => {
        let locations = []

        result.forEach((location) => {
          locations.push(location.data())
        });
        resolve(locations)
      })
  })

}
module.exports =  {
  addUser : addUser,
  addData:addData,
  getData: getData
}