$('#postData').on('click', () => {
    $("p").remove(".deletePost")
    axios.post('api/locations', {
        userId: $('#userId').val(),
        date: $('#date').val(),
        latitude: $('#latitude').val(),
        longitude: $('#longitude').val(),
        battery: $('#battery').val(),
        speed: $('#speed').val(),
        direction: $('#direction').val(),
    })
        .then((res) => {
            $('.postData').append(`<p class='deletePost'>${res.data.success ? "locations envoyer" : "une erreur s'est produit lors de l'envoi " + JSON.stringify(res.data)}</p>`)
        })
})


$('#getData').on('click', () => {
    $("tr , div").remove(".delete")
    axios.get('api/locations', {
        params: {
            locationNumber: $('#locationNumber').val()
        }
    })
        .then((res) => {
            let result = res.data.locations

            if (res.data.success) {
                $('.tabResult').css('display', 'contents')
                result.forEach((value) => {
                    $('.tabGetResult').append(`
                    <tr class='delete'>
                        <td>${value.date}</td>
                        <td>${value.latitude}</td>
                        <td>${value.longitude}</td>
                        <td>${value.direction}</td>
                        <td>${value.speed}</td>
                        <td>${value.battery}</td>
                    </tr>
                    `)
                });
            } else
                $("#btnGetData")
                    .append(`
                        <div class="delete">
                        <p>${"une erreur s'est produit lors de l'envoi " + JSON.stringify(res.data)}</p>
                        </div>
                    `)
        })
})